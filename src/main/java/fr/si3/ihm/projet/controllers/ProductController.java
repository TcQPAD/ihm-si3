package fr.si3.ihm.projet.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Defines the controller
 * for the "Nos Produits" view
 * of the application
 */
public class ProductController extends SuperController {

    @FXML private VBox vboxExclusive;

    @FXML private VBox vboxMenus;
    @FXML private VBox vboxEntrees;
    @FXML private VBox vboxPlats;
    @FXML private VBox vboxDesserts;

    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        vboxExclusive.setOnMouseClicked(event -> updateViewCenter("/fxml/productExclusiveView.fxml", "Produits phares"));

        vboxMenus.setOnMouseClicked(event -> updateViewCenter("/fxml/productMenuView.fxml", ""));

        vboxEntrees.setOnMouseClicked(event -> updateViewCenter("/fxml/productStarterView.fxml", ""));

        vboxPlats.setOnMouseClicked(event -> updateViewCenter("/fxml/productMealView.fxml", ""));

        vboxDesserts.setOnMouseClicked(event -> updateViewCenter("/fxml/productFinisherView.fxml", ""));
    }
}
