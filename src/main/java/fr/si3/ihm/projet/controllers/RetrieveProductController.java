package fr.si3.ihm.projet.controllers;

import fr.si3.ihm.projet.models.CustomerOrderModel;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Defines the controller for the "Récupérer un produit" view.
 */
public class RetrieveProductController extends SuperController implements IDataGeneratorController{

    /* Order fields */
    @FXML private Label orderProduct;
    @FXML private Label orderDescription;
    @FXML private Label orderCustomer;
    @FXML private Label orderPrice;
    @FXML private Label orderDate;
    @FXML private Label orderKey;

    @FXML private ImageView orderImage;

    /* Help container, text and image bindings */
    @FXML private HBox helpHBox;
    @FXML private ImageView helpImage;
    @FXML private Label helpLabel;

    /*  */
    @FXML private Button buttonSendMail;
    @FXML private Label textSendMail;
    @FXML public TextField orderInput;

    private CustomerOrderModel orderModel;


    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        finalizeView();
    }

    /**
     * Initializes the listeners of this view
     */
    private void initViewListeners() {
        helpHBox.setOnMouseEntered(event -> {
            helpImage.setImage(new Image("/images/info-hover.png"));
            helpLabel.setTextFill(Paint.valueOf("#666365"));
        });

        helpHBox.setOnMouseExited(event -> {
            helpImage.setImage(new Image("/images/info.png"));
            helpLabel.setTextFill(Paint.valueOf("#000000"));
        });

        buttonSendMail.setOnMouseExited(event -> textSendMail.setStyle("-fx-font-family: \"Lato\"; -fx-text-fill: #47b475; -fx-font-weight: bold; -fx-font-size: 20px;"));

        buttonSendMail.setOnMouseEntered(event -> textSendMail.setStyle("-fx-font-family: \"Lato\"; -fx-text-fill: #FFFFFF; -fx-font-weight: bold; -fx-font-size: 20px;"));

        buttonSendMail.setOnMouseClicked(event -> displayStoreData(false));

        orderInput.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                displayStoreData(false);
            }
        });
    }

    /**
     * Displays the next or previous data, depending on the value of the b boolean,
     * in the data structure gotten from the controller
     * model.
     * <p>
     * b = false ==> next 6 data     (iterator incremented)
     * b = true  ==> previous 6 data (iterator decremented)
     *
     * @param b telling wether the next data to display must appear
     *          from left (false) or true (right)
     */
    @Override
    public void displayStoreData(boolean b) {

        Notifications notifications;
        ImageView icon;

        JSONObject order = orderModel.getOrder(orderInput.getText());

        if (order != null) {
            orderImage.setImage(new Image(order.getString(CustomerOrderModel.ORDER_IMAGE)));

            orderProduct.setText(order.getString(CustomerOrderModel.ORDER_PRODUCT));
            orderDescription.setText(order.getString(CustomerOrderModel.ORDER_DESCRIPTION));
            orderCustomer.setText(order.getString(CustomerOrderModel.ORDER_CUSTOMER));
            orderPrice.setText(order.getString(CustomerOrderModel.ORDER_PRICE));
            orderDate.setText(order.getString(CustomerOrderModel.ORDER_DATE));
            orderKey.setText(order.getString(CustomerOrderModel.ORDER_KEY));

            icon = new ImageView(new Image("/images/checked.png"));
            notifications = Notifications.create()
                    .title("Commande chargée")
                    .text("Votre commande a été chargée. Sophia' Miam vous remercie de votre confiance.")
                    .graphic(icon)
                    .hideAfter(Duration.seconds(5))
                    .darkStyle()
                    .position(Pos.BOTTOM_RIGHT);

            notifications.show();

        }
        else {
            icon = new ImageView(new Image("/images/cancel.png"));
            notifications = Notifications.create()
                    .title("Commande inexistante")
                    .text("Le numéro de commande que vous avez entré est invalide, ou bien la commande" +
                            "a été supprimée.")
                    .graphic(icon)
                    .hideAfter(Duration.seconds(5))
                    .darkStyle()
                    .position(Pos.BOTTOM_RIGHT);

            notifications.show();
        }
    }

    /**
     * Configuration method, used to
     * initialize all data structures needed in the algorithm
     * {@link this#displayStoreData(boolean)}
     */
    @Override
    public void initDataStructures() {
        orderModel = new CustomerOrderModel();
    }

    /**
     * Finalizes the view display.
     * You may want to call other methods than
     * the ones defined in {@link IDataGeneratorController} interface.
     * <p>
     * This method should be the only one method from
     * {@link IDataGeneratorController} invoked in the controllers,
     * thus you should put all final calls here.
     */
    @Override
    public void finalizeView() {
        initViewListeners();
        initDataStructures();
    }
}
