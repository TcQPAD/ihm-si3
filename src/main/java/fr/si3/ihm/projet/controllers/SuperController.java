package fr.si3.ihm.projet.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Implements the {@link Initializable} interface
 * to make all its children implement it by
 * child class relation
 *
 * Also defines the methods common to all controlers :
 * <ul>
 *     <li>{@link this#changeView(String, String)}</li>
 *     <li>{@link this#updateViewCenter(String, String)}</li>
 * </ul>
 */
public abstract class SuperController implements Initializable {

    @FXML private BorderPane rootContainer;
    @FXML protected Pane subContainer;

    @FXML protected MenuBar menuBar;

    /* Menu bar top level bindings */
    @FXML protected Label homeViewLabel;
    @FXML protected ImageView homeViewImage;
    @FXML private Label productView;
    @FXML private Menu topMenuProduct;
    @FXML public Menu aboutView;
    @FXML public Label aboutViewLabel;
    @FXML private Label findUsView;
    @FXML private Menu topMenuAbout;
    @FXML private Label topMenuDiscounts;

    /* Menu bar items bindings */
    @FXML private MenuItem productExclusiveView;
    @FXML private MenuItem productMenuView;
    @FXML private MenuItem productStarterView;
    @FXML private MenuItem productMealView;
    @FXML private MenuItem productFinisherView;
    @FXML private MenuItem retrieveProductView;

    /* Bottom hyper links bindings */
    @FXML private Hyperlink contactView;
    @FXML private Hyperlink mentionsView;

    private FXMLLoader fxmlLoader;

    /**
     * Changes the current view to
     * display the new view viewFxml
     *
     * @param viewFxml the new view to display
     *                 on the stage
     */
    protected void changeView(String viewFxml, String stageTitle) {

        fxmlLoader = new FXMLLoader();
        Parent rootNode = null;
        try {
            rootNode = fxmlLoader.load(getClass().getResourceAsStream(viewFxml));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stage = (Stage) rootContainer.getScene().getWindow();
        Scene scene = new Scene(rootNode, rootContainer.getScene().getWidth(), rootContainer.getScene().getHeight());
        scene.getStylesheets().add("/styles/styles.css");
        stage.setTitle(stageTitle);
        stage.setScene(scene);

        stage.setOnCloseRequest(event -> System.exit(0));

    }

    /**
     * Initialises the Listeners for the menu
     * bar, since it's a node common to all views
     *
     */
    void initMenuBarListeners() {

        // Listeners for the home view call
        homeViewImage.setOnMouseClicked(event -> changeView("/fxml/homeView.fxml", "Accueil"));

        homeViewLabel.setOnMouseClicked(event -> changeView("/fxml/homeView.fxml", "Accueil"));

        // Listener for the product view call
        productView.setOnMouseEntered(event -> topMenuProduct.show());

        productView.setOnMouseClicked(event -> updateViewCenter("/fxml/productView.fxml", "Produits"));

        // Submenu listeners
        aboutViewLabel.setOnMouseEntered(event -> aboutView.show());
        findUsView.setOnMouseEntered(event -> topMenuAbout.show());

        findUsView.setOnMouseClicked(event -> updateViewCenter("/fxml/findUsView.fxml", "Nous Trouver"));

        topMenuDiscounts.setOnMouseClicked(event -> updateViewCenter("/fxml/discountView.fxml", "Bons plans"));

        productExclusiveView.setOnAction(event -> updateViewCenter("/fxml/productExclusiveView.fxml", "Produits phares"));

        productMenuView.setOnAction(event -> updateViewCenter("/fxml/productMenuView.fxml", "Menus"));

        productStarterView.setOnAction(event -> updateViewCenter("/fxml/productStarterView.fxml", "Entrées"));

        productMealView.setOnAction(event -> updateViewCenter("/fxml/productMealView.fxml", "Plats"));

        productFinisherView.setOnAction(event -> updateViewCenter("/fxml/productFinisherView.fxml", "Desserts"));

        retrieveProductView.setOnAction(event -> updateViewCenter("/fxml/retrieveProductView.fxml", "Récupérer un produit"));

        contactView.setOnMouseClicked(event -> updateViewCenter("/fxml/contactView.fxml", "Nous contacter"));

        mentionsView.setOnMouseClicked(event -> updateViewCenter("/fxml/mentionsView.fxml", "Mentions Légales"));
    }

    /**
     * Updates view in the center of the BorderPane
     * binded to {@link this#rootContainer}.
     *
     * If the view to display is the home view,
     * the left and right borders of the BorderPane
     * should be updated as well.
     * That's why the {@link this#changeView(String, String)} should
     * be called instead.
     *
     * Thus it is obvious that if the last view was the
     * home view, the left and right borders should be cleared.
     *
     * @param fxmlView the new view to display in the center
     *                 of the root BorderPane
     */
    void updateViewCenter(String fxmlView, String stageTitle) {

        fxmlLoader = new FXMLLoader();
        try {
            // if current view is not home view
            // we update the value of rootContainer to bind it to
            // the root BorderPane
            if (rootContainer == null)
                rootContainer = (BorderPane) subContainer.getParent();

                // clear left/right borders if not null
                if (rootContainer.getRight() != null && rootContainer.getLeft() != null) {
                    rootContainer.getChildren().remove(rootContainer.getRight());
                    rootContainer.getChildren().remove(rootContainer.getLeft());
                }


                rootContainer.getChildren().remove(rootContainer.getCenter());
                rootContainer.setCenter(fxmlLoader.load(getClass().getResourceAsStream(fxmlView)));

            ((Stage) rootContainer.getScene().getWindow()).setTitle(stageTitle);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
