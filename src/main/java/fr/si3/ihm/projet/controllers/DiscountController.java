package fr.si3.ihm.projet.controllers;

import fr.si3.ihm.projet.models.DiscountModel;
import javafx.animation.*;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

import static javafx.animation.Animation.INDEFINITE;

/**
 * Defines the controller
 * for the "Bons Plans" view
 * of the application
 */

public class DiscountController extends SuperAnimator implements INavigationController {

    private DiscountModel model;

    @FXML private Label imageSliderTitle;
    @FXML private Label imageCarouselTitle;

    @FXML private Hyperlink discount1;
    @FXML private Hyperlink discount2;
    @FXML private Hyperlink discount3;
    @FXML private Hyperlink discount4;

    @FXML private ImageView discountImage;

    @FXML private ImageView partnerImage1;
    @FXML private ImageView partnerImage2;
    @FXML private ImageView partnerImage3;
    @FXML private ImageView partnerImage4;
    @FXML private ImageView partnerImage5;
    @FXML private ImageView partnerImage6;
    @FXML private Region partnerRegion;

    private int currentDiscount = 0;

    // Declared static so we can stop the previous one, or it won't stop, won't get gc(), and will mess up the anim
    private static Timeline timeline;

    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        model = new DiscountModel();

        setUpPartners();
        initTitles();

        initNavigationNode();
    }

    /**
     * Initializes all listeners for the
     * navigation node, needed to make the user
     * able to browse data.
     *
     * One may want to add his own listener in
     * controllers inheriting classes which implement
     * this interface.
     */
    @Override
    public void initNavigationNode() {
        initDiscounts();

        ListChangeListener<? super Image> listener = c -> {
            if (c.getList().size() >= 6)
                updatePartners();
        };

        model.addListener(listener);

        if(timeline != null)
            timeline.stop();

        timeline = new Timeline(new KeyFrame(Duration.seconds(5), event -> doASwap()));
        timeline.setCycleCount(INDEFINITE);
        timeline.play();

    }

    private void initDiscounts() {
        discountImage.setImage(model.getDiscountImage(0));
        discount1.setText(model.getDiscountText(0));
        discount1.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            updateDiscountImage(0);
        });
        discount2.setText(model.getDiscountText(1));
        discount2.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            updateDiscountImage(1);
        });
        discount3.setText(model.getDiscountText(2));
        discount3.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            updateDiscountImage(2);
        });
        discount4.setText(model.getDiscountText(3));
        discount4.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            updateDiscountImage(3);
        });
    }

    private void initTitles() {
        imageSliderTitle.setText(model.getImageSliderTitleText());
        imageCarouselTitle.setText(model.getImageCarouselTitleText());
    }

    private void doASwap() {
        model.addPartner(model.removePartner(0));
    }

    // The lefter image blinks for some mysterious reason. It shouldn't since all the animations are launched at the same time, have the same duration, and the others does not
    private void updatePartners() {
        int size = (int) partnerImage2.getFitWidth();
        int spacement = (int) partnerRegion.getWidth();

        ParallelTransition t1 = new ParallelTransition(
                getBorderCarTrans(size),
                getFirstBasicCarTrans(partnerImage2, size, spacement, Duration.millis(400), event -> partnerImage2.setImage(model.getPartners().get(1))),
                getFirstBasicCarTrans(partnerImage3, size, spacement, Duration.millis(400), event -> partnerImage3.setImage(model.getPartners().get(2))),
                getFirstBasicCarTrans(partnerImage4, size, spacement, Duration.millis(400), event -> partnerImage4.setImage(model.getPartners().get(3))),
                getFirstBasicCarTrans(partnerImage5, size, spacement, Duration.millis(400), event -> partnerImage5.setImage(model.getPartners().get(4))),
                getFirstBasicCarTrans(partnerImage6, size, spacement, Duration.millis(400), event -> partnerImage6.setImage(model.getPartners().get(5)))
        );

        ParallelTransition t2 = new ParallelTransition(
                getSecondBasicCarTrans(partnerImage2),
                getSecondBasicCarTrans(partnerImage3),
                getSecondBasicCarTrans(partnerImage4),
                getSecondBasicCarTrans(partnerImage5),
                getSecondBasicCarTrans(partnerImage6)
        );

        t1.setOnFinished(event -> t2.play());

        t1.play();
    }

    private void setUpPartners() {
        partnerImage1.setImage(model.getPartners().get(0));
        partnerImage2.setImage(model.getPartners().get(1));
        partnerImage3.setImage(model.getPartners().get(2));
        partnerImage4.setImage(model.getPartners().get(3));
        partnerImage5.setImage(model.getPartners().get(4));
        partnerImage6.setImage(model.getPartners().get(5));
    }

    private void updateDiscountImage(int i) {
        if(i > currentDiscount)
            sliderSwap(discountImage, model.getDiscountImage(i), -600, Duration.millis(200), event -> lockNavigationNodes(false)).play();
        else if(i < currentDiscount)
            sliderSwap(discountImage, model.getDiscountImage(i), 600, Duration.millis(200), event -> lockNavigationNodes(false)).play();
        else
            lockNavigationNodes(false);
        this.currentDiscount = i;
    }

    private ParallelTransition getBorderCarTrans(int size) {

        TranslateTransition t1 = new TranslateTransition(Duration.millis(199), partnerImage1);
        t1.setByX(-100);

        FadeTransition t2 = new FadeTransition(Duration.millis(199), partnerImage1);
        t2.setFromValue(1);
        t2.setToValue(0);

        ParallelTransition p1 = new ParallelTransition(partnerImage1, t1, t2);

        TranslateTransition t3 = new TranslateTransition(Duration.millis(1), partnerImage1);
        t3.setByX(100*7 + size*5);

        FadeTransition t4 = new FadeTransition(Duration.millis(200), partnerImage1);
        t4.setFromValue(0);
        t4.setToValue(1);

        TranslateTransition t5 = new TranslateTransition(Duration.millis(200), partnerImage1);
        t5.setByX(-100);

        ParallelTransition p2 = new ParallelTransition(partnerImage1, t5, t4);

        FadeTransition t6 = new FadeTransition(Duration.millis(1), partnerImage1);
        t6.setFromValue(1);
        t6.setToValue(0);

        TranslateTransition t7 = new TranslateTransition(Duration.millis(1), partnerImage1);
        t7.setByX(-(100*5 + size*5));

        FadeTransition t8 = new FadeTransition(Duration.millis(1), partnerImage1);
        t8.setFromValue(0);
        t8.setToValue(1);

        p1.setOnFinished(event -> {
            partnerImage1.setImage(model.getPartners().get(5));
            t3.play();
        });

        t3.setOnFinished(event -> p2.play());

        p2.setOnFinished(event -> t6.play());

        t6.setOnFinished(event -> t7.play());

        t7.setOnFinished(event -> {
            partnerImage1.setImage(model.getPartners().get(0));
            t8.play();
        });

        return p1;
    }

    /**
     * Locks the navigation nodes to disable the possibility
     * to click on it, when reaching a bound of data.
     *
     * The possibility to click is represented by the given boolean,
     * i.e., false to lock it and true to unlock it.
     *
     * @param lock the value representing the lock state of the nodes
     *             to which it is applied
     */
    @Override
    public void lockNavigationNodes(boolean lock) {
        discount1.setDisable(lock);
        discount2.setDisable(lock);
        discount3.setDisable(lock);
        discount4.setDisable(lock);
    }

    /**
     * Updates the nodes used to navigate (hyperlinks, images, buttons)
     * whenever the displayStoreData method is invoked.
     *
     * You should use this method to enable/disable nodes and
     * make them visible/invisible.
     *
     */
    @Override
    public void updateNavigationNode() {
        //Not implemented because not needed here
    }
}
