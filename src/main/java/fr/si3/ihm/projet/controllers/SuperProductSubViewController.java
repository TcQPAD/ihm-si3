package fr.si3.ihm.projet.controllers;

import fr.si3.ihm.projet.models.ProductModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import org.json.JSONObject;

/**
 * Defines the controller
 * for the "Nos Produits > Menus" view
 * of the application
 */
public abstract class SuperProductSubViewController extends SuperAnimator implements IDataGeneratorController, INavigationController {

    /* navigation of the view */
    @FXML protected HBox backToProducts;
    @FXML protected ImageView backToProductsImage;
    @FXML protected Label backToProductsLabel;

    /* images to refresh */
    @FXML protected ImageView subViewImage1;
    @FXML protected ImageView subViewImage2;
    @FXML protected ImageView subViewImage3;
    @FXML protected ImageView subViewImage4;
    @FXML protected ImageView subViewImage5;
    @FXML protected ImageView subViewImage6;

    /* labels to refresh */
    @FXML protected Label subViewLabel1;
    @FXML protected Label subViewLabel2;
    @FXML protected Label subViewLabel3;
    @FXML protected Label subViewLabel4;
    @FXML protected Label subViewLabel5;
    @FXML protected Label subViewLabel6;

    /* Product browsing tools */
    @FXML public ImageView nextPage;
    @FXML public ImageView previousPage;
    @FXML public Label pageCounterLabel;

    /* Binded variables containers */
    private ImageView[] subViewImages;
    private Label[] subViewLabels;


    /* Data productModel */
    ProductModel productModel;

    /* Pagination indexes */
    protected int totalPage;
    protected int pageCounter;

    /**
     * Initializes the listeners of the view
     * and all objects
     *
     */
    private void initListeners() {
        backToProducts.setOnMouseEntered(event -> {
            backToProductsImage.setImage(new Image("/images/back-hover.png"));
            backToProductsLabel.setTextFill(Paint.valueOf("#ffffff"));
        });

        backToProducts.setOnMouseExited(event -> {
            backToProductsImage.setImage(new Image("/images/back.png"));
            backToProductsLabel.setTextFill(Paint.valueOf("#000000"));
        });

        backToProducts.setOnMouseClicked(event -> {
            updateViewCenter("/fxml/productView.fxml", "Produits");
        });
    }

    /**
     * Configuration method, used to
     * initialize all data structures needed in the algorithm
     * {@link this#displayStoreData(boolean)}
     */
    @Override
    public void initDataStructures() {
        subViewImages = new ImageView[] {subViewImage1, subViewImage2, subViewImage3, subViewImage4, subViewImage5, subViewImage6};
        subViewLabels = new Label[] {subViewLabel1, subViewLabel2, subViewLabel3, subViewLabel4, subViewLabel5, subViewLabel6};

        // page counters
        pageCounter = 0;
        totalPage = productModel.getData().length()/6;
        totalPage += productModel.getData().length() % 6 == 0 ? 0 : 1;
    }

    /**
     * Locks the navigation nodes to disable the possibility
     * to click on it, when reaching a bound of data.
     *
     * The possibility to click is represented by the given boolean,
     * i.e., false to lock it and true to unlock it.
     *
     * @param lock the value representing the lock state of the nodes
     *             to which it is applied
     */
    @Override
    public void lockNavigationNodes(boolean lock) {
        previousPage.setDisable(lock);
        nextPage.setDisable(lock);
    }

    /**
     * Updates the nodes used to navigate (hyperlinks, images, buttons)
     * whenever the displayStoreData method is invoked.
     *
     * You should use this method to enable/disable nodes and
     * make them visible/invisible.
     *
     */
    @Override
    public void updateNavigationNode() {
        // reset the product counter to avoid JSONException
        // and update image listeners accessibility in order to
        // display a better view
        if (pageCounter == totalPage) {
            nextPage.setDisable(true);
        }
        else
            nextPage.setDisable(false);

        if (pageCounter == 1) {
            previousPage.setDisable(true);
        }
        else
            previousPage.setDisable(false);
    }

    /**
     * Displays the next or previous data, depending on the value of the b boolean,
     * in the data structure gotten from the controller
     * productModel.
     * <p>
     * b = false ==> next 6 data     (iterator incremented)
     * b = true  ==> previous 6 data (iterator decremented)
     *
     * @param b the boolean informing whether
     *          the user clicked left or right
     */
    @Override
    public void displayStoreData(boolean b) {
        JSONObject data;

        pageCounter += b ? -1 : 1;
        // sets the loop condition in function of the left value
        boolean canLoop = (pageCounter-1) * 6 < (productModel.getData().length());

        for (int i = 0; i <= 5 && canLoop; i++) {

            // get data and move data pointer
            data = productModel.getData().getJSONObject((pageCounter-1)*6 + i);

            canLoop = (pageCounter-1)*6 + i + 1 < (productModel.getData().length());

            // update view
            subViewImages[i].setImage(new Image(data.getString(ProductModel.PRODUCT_IMAGE)));
            subViewLabels[i].setText(data.getString(ProductModel.PRODUCT_NAME));
        }
        updateNavigationNode();
    }

    /**
     * Finalizes the view display.
     * You may want to call other methods than
     * the ones defined in {@link IDataGeneratorController} interface.
     * <p>
     * This method should be the only one method from
     * {@link IDataGeneratorController} invoked in the controllers,
     * thus you should put all final calls here.
     */
    @Override
    public void finalizeView() {
        if (!productModel.getOpenedFile().equals(ProductModel.EXCLU_JSON_FILE_NAME))
            initListeners();
        initDataStructures();
        displayStoreData(false);

        // first data layout done, we set page number to the good value
        pageCounterLabel.setText("Page " + pageCounter + "/" + totalPage);
    }
}
