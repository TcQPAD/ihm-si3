package fr.si3.ihm.projet.controllers;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Defines the controller
 * of the "Mentions Légales" view
 */
public class MentionsController extends SuperController {

    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
