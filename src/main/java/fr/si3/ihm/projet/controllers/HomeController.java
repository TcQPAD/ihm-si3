package fr.si3.ihm.projet.controllers;

import fr.si3.ihm.projet.models.ResearchModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import org.controlsfx.control.textfield.TextFields;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Defines the controller
 * for the "Accueil" view of the application
 */
public class HomeController extends SuperAnimator implements INavigationController {

    @FXML private ImageView previousImage;
    @FXML private ImageView nextImage;
    @FXML private ImageView carouselImage;

    @FXML private TextField searchField;
    @FXML private Button searchButton;

    // Research model, contains research data
    private ResearchModel researchModel;

    private int imagePicker;
    private List<String> slideImages;

    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initMenuBarListeners();
        initSearchBar();
        populateCarousel();
        initNavigationNode();
    }

    private void initSearchBar() {
        researchModel = new ResearchModel();

        TextFields.bindAutoCompletion(searchField, researchModel.getViewNamesWithFxml().keySet());

        searchButton.setOnMouseClicked(event -> processResearch());
        searchField.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                processResearch();
            }
        });
    }

    /**
     * Starts the research process, by
     * using the map of keywords/views
     * stored in {@link ResearchModel}
     */
    private void processResearch() {
        String research = searchField.getText();
        String fxmlPath = researchModel.getFxmlResearched(research);

        if (fxmlPath != null)
            updateViewCenter(fxmlPath, research);
        else
            System.out.println("Not found!");
    }

    /**
     * Populates the {@link ArrayList} storing the images
     * to display in the carousel
     */
    private void populateCarousel() {
        slideImages = new ArrayList<>();
        slideImages.add("/images/presentation_burger.jpg");
        slideImages.add("/images/burger_product.jpg");
        slideImages.add("/images/burger_friends.jpg");

        imagePicker = 0;
    }

    /**
     * Initializes all listeners for the
     * navigation node, needed to make the user
     * able to browse data.
     *
     * One may want to add his own listener in
     * controllers inheriting classes which implement
     * this interface.
     */
    @Override
    public void initNavigationNode() {
        previousImage.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            // change with some image you got on your computer bro
            //slide(leftImage(), -1);
            sliderSwap(carouselImage, leftImage(), 600, Duration.millis(200), event2 -> lockNavigationNodes(false)).play();
            //carouselImage.setImage(leftImage());
        });

        nextImage.setOnMouseClicked(event -> {
            lockNavigationNodes(true);
            // change with some image you got on your computer bro
            //slide(rightImage(), 1);
            sliderSwap(carouselImage, rightImage(), -600, Duration.millis(200), event2 -> lockNavigationNodes(false)).play();
            //carouselImage.setImage(rightImage());
        });
    }

    /**
     * Locks the navigation nodes to disable the possibility
     * to click on it, when reaching a bound of data.
     *
     * The possibility to click is represented by the given boolean,
     * i.e., false to lock it and true to unlock it.
     *
     * @param lock the value representing the lock state of the nodes
     *             to which it is applied
     */
    @Override
    public void lockNavigationNodes(boolean lock) {
        previousImage.setDisable(lock);
        nextImage.setDisable(lock);
    }

    /**
     * Returns the image to display
     * when clicking left of the carousel
     * @return the new image to display
     */
    private Image leftImage() {

        imagePicker--;

        // no more pictures on left
        if (imagePicker < 0) {
            imagePicker = slideImages.size() - 1;
        }
        return new Image(slideImages.get(imagePicker));
    }

    /**
     * Returns the image to display
     * when clicking right of the carousel
     * @return the new image to display
     */
    private Image rightImage() {

        imagePicker++;

        // no more pictures on right
        if (imagePicker > slideImages.size() - 1) {
            imagePicker = 0;
        }
        return new Image(slideImages.get(imagePicker));
    }

    /**
     * Updates the nodes used to navigate (hyperlinks, images, buttons)
     * whenever the displayStoreData method is invoked.
     *
     * You should use this method to enable/disable nodes and
     * make them visible/invisible.
     *
     */
    @Override
    public void updateNavigationNode() {
        //Not implemented because not needed here
    }
}
