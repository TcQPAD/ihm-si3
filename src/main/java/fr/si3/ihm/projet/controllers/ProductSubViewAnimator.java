package fr.si3.ihm.projet.controllers;

import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * Created by maxim on 06/03/2017.
 */
public abstract class ProductSubViewAnimator extends SuperProductSubViewController {

    /**
     * Initializes all listeners for the
     * navigation node, needed to make the user
     * able to browse data.
     *
     * One may want to add his own listener in
     * controllers inheriting classes which implement
     * this interface.
     */
    @Override
    public void initNavigationNode() {
        previousPage.setOnMouseClicked(event -> {
            if(pageCounter>1) {
                lockNavigationNodes(true);
                playTransition(true);
            }
        });
        nextPage.setOnMouseClicked(event -> {
            if(pageCounter < totalPage) {
                lockNavigationNodes(true);
                playTransition(false);
            }
        });
    }

    void playTransition(boolean b) {
        ParallelTransition p1 = new ParallelTransition(
                createFirstTransition(b, subViewImage1, subViewLabel1),
                createFirstTransition(b, subViewImage2, subViewLabel2),
                createFirstTransition(b, subViewImage3, subViewLabel3),
                createFirstTransition(b, subViewImage4, subViewLabel4),
                createFirstTransition(b, subViewImage5, subViewLabel5),
                createFirstTransition(b, subViewImage6, subViewLabel6)
        );

        ParallelTransition p2 = new ParallelTransition(
                createSecondTransition(b, subViewImage1, subViewLabel1),
                createSecondTransition(b, subViewImage2, subViewLabel2),
                createSecondTransition(b, subViewImage3, subViewLabel3),
                createSecondTransition(b, subViewImage4, subViewLabel4),
                createSecondTransition(b, subViewImage5, subViewLabel5),
                createSecondTransition(b, subViewImage6, subViewLabel6)
        );

        p1.setOnFinished(event -> {
            displayStoreData(b);
            p2.play();
        });

        p2.setOnFinished(event -> {
            lockNavigationNodes(false);
            pageCounterLabel.setText("Page " + pageCounter + "/" + totalPage);
            updateNavigationNode();
        });

        p1.play();
    }

    private Transition createFirstTransition(boolean b, ImageView imageView, Label label) {

        Transition t1 = new ParallelTransition(
                translateAndFade(imageView, b ? 200 : -200, false, Duration.millis(200)),
                translateAndFade(label, b ? 200 : -200, false, Duration.millis(200))
        );

        t1.setOnFinished(event -> new ParallelTransition(
                translate(imageView, b ? -400 : 400, Duration.millis(1)),
                translate(label, b ? -400 : 400, Duration.millis(1))
        ).play());

        return t1;
    }

    private Transition createSecondTransition(boolean b, ImageView imageView, Label label) {
        return new ParallelTransition(
                translateAndFade(imageView, b ? 200 : -200, true, Duration.millis(200)),
                translateAndFade(label, b ? 200 : -200, true, Duration.millis(200))
        );
    }
}
