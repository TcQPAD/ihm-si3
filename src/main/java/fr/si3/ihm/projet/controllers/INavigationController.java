package fr.si3.ihm.projet.controllers;

/**
 * Defines 3 new functions for
 * controllers which handle views with navigation
 * (like carousels, books etc...).
 */
public interface INavigationController {

    /**
     * Initializes all listeners for the
     * navigation node, needed to make the user
     * able to browse data.
     *
     * One may want to add his own listener in
     * controllers inheriting classes which implement
     * this interface.
     */
    void initNavigationNode();

    /**
     * Locks the navigation nodes to disable the possibility
     * to click on it, when reaching a bound of data.
     *
     * The possibility to click is represented by the given boolean,
     * i.e., false to lock it and true to unlock it.
     *
     * @param lock the value representing the lock state of the nodes
     *             to which it is applied
     */
    void lockNavigationNodes(boolean lock);

    /**
     * Updates the nodes used to navigate (hyperlinks, images, buttons)
     * whenever the displayStoreData method is invoked.
     *
     * You should use this method to enable/disable nodes and
     * make them visible/invisible.
     *
     */
    void updateNavigationNode();
}
