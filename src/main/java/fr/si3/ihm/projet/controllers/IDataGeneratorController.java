package fr.si3.ihm.projet.controllers;

/**
 * Defines 3 function for all controllers
 * who display data programmatically.
 *
 */
public interface IDataGeneratorController {

    /**
     * Displays the next or previous data, depending on the value of the b boolean,
     * in the data structure gotten from the controller
     * model.
     *
     * b = false ==> next 6 data     (iterator incremented)
     * b = true  ==> previous 6 data (iterator decremented)
     */
    void displayStoreData(boolean b);

    /**
     * Configuration method, used to
     * initialize all data structures needed in the algorithm
     * {@link this#displayStoreData(boolean)}
     */
    void initDataStructures();

    /**
     * Finalizes the view display.
     * You may want to call other methods than
     * the ones defined in {@link IDataGeneratorController} interface.
     *
     * This method should be the only one method from
     * {@link IDataGeneratorController} invoked in the controllers,
     * thus you should put all final calls here.
     */
    void finalizeView();
}
