package fr.si3.ihm.projet.controllers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;

import javafx.fxml.FXML;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Defines the controller for the "Nous Trouver" view.
 */
public class FindUsController extends SuperController implements MapComponentInitializedListener {

    @FXML public HBox googleMapContainer;

    private GoogleMapView mapView;
    private GoogleMap map;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        displayMap();
    }

    /**
     * Uses the {@link com.lynden.gmapsfx.GoogleMapView} class from the lynden
     * maven repository to set up a GoogleMap map and display it.
     *
     * See: <a>https://rterp.wordpress.com/2014/04/25/gmapsfx-add-google-maps-to-your-javafx-application/</a>
     */
    private void displayMap() {

        //Create the JavaFX component and set this as a listener so we know when
        //the map has been initialized, at which point we can then begin manipulating it.
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(this);

        googleMapContainer.getChildren().add(mapView);
    }

    @Override
    public void mapInitialized() {
        //Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(43.6155763, 7.0695305))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(true)
                .zoomControl(true)
                .zoom(12);

        map = mapView.createMap(mapOptions);

        //Add a marker to the map
        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position( new LatLong(43.6155763, 7.0695305) )
                .visible(Boolean.TRUE)
                .animation(Animation.BOUNCE)
                .title("Sophia'Miam");

        Marker marker = new Marker( markerOptions );

        map.addMarker(marker);
    }
}
