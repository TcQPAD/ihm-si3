package fr.si3.ihm.projet.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

/**
 * Defines the controller
 * of the "Nous Contacter" view
 */
public class ContactController extends SuperController {

    /* Variable binded to mail button control */
    @FXML private Button buttonSendMail;
    @FXML private ImageView imageSendMail;
    @FXML private Label textSendMail;

    /* Message fields */
    @FXML private TextField contactLastName;
    @FXML private TextField contactFirstName;
    @FXML private TextField contactEmail;
    @FXML private TextArea contactMessage;

    /* Fields used to check/send a mail */
    private static final Pattern VALID_EMAIL = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final String ENCODING = "UTF-8";
    private static final String PHP_URL = "http://46.101.194.82/ihm_si3_message.php";

    /**
     * Called to initialize a controller after its root element
     * has been completely processed.
     *
     * @param location The location used to resolve relative paths for the root object,
     *                 or null if the location is not known.
     * @param resources The resources used to localize the root object,
     *                  or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        buttonSendMail.setOnMouseExited(event -> {
            imageSendMail.setImage(new Image("/images/sent-mail.png"));
            textSendMail.setStyle("-fx-font-family: \"Lato\"; -fx-text-fill: #47b475; -fx-font-weight: bold; -fx-font-size: 20px;");
        });

        buttonSendMail.setOnMouseEntered(event -> {
            imageSendMail.setImage(new Image("/images/sent-mail-hover.png"));
            textSendMail.setStyle("-fx-font-family: \"Lato\"; -fx-text-fill: #FFFFFF; -fx-font-weight: bold; -fx-font-size: 20px;");
        });

        // defines the listener for mail sending
        buttonSendMail.setOnMouseClicked(event -> {
            doSendMessage();
        });
    }

    /**
     * Checks that the user entered good data
     * @return true if the user entered good data
     */
    private boolean checkUserData() {
        return !(areTextFieldNotFilled() || userEmailBad());
    }

    /**
     * Checks that the given email is a valid email, using
     * regex validation
     *
     * See <a src="http://stackoverflow.com/questions/8204680/java-regex-email">Validate Email with Regex</a>
     *
     * @return true if the email entered is valid
     */
    private boolean userEmailBad() {
        return !VALID_EMAIL.matcher(contactEmail.getText()).find();
    }

    private boolean areTextFieldNotFilled() {
        return "".equals(contactLastName.getText()) || "".equals(contactFirstName.getText())
                || "".equals(contactEmail.getText()) || "".equals(contactMessage.getText());
    }

    /**
     * This method sends the entered information
     * to a distant database.
     *
     * <b>Important:</b> the validity and integrity
     * of the information should be checked before invoking this method
     *
     * If the operation was successful, this method returns true
     *
     * @return true if the operation was successful, or false if
     *         a problem occurs
     */
    private boolean sendMessage() {

        try {

            String dataMessage = URLEncoder.encode("contactLastName", ENCODING)
                    + "=" + URLEncoder.encode(contactLastName.getText(), ENCODING);

            dataMessage += URLEncoder.encode("contactFirstName", ENCODING)
                    + "=" + URLEncoder.encode(contactFirstName.getText(), ENCODING);

            dataMessage += URLEncoder.encode("contactEmail", ENCODING)
                    + "=" + URLEncoder.encode(contactEmail.getText(), ENCODING);

            dataMessage += URLEncoder.encode("contactMessage", ENCODING)
                    + "=" + URLEncoder.encode(contactMessage.getText(), ENCODING);

            /* Let's open a connection to the PHP script */
            URL           url  = new URL(PHP_URL);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);

            /* We create an object which will write the dataMessage on the connection stream */
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(dataMessage);
            wr.flush();


            /* We finished sending data, now let's listen to PHP response */
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String        line;

            // If PHP answered
            if ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            if ("0".equals(sb.toString()))
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        // PHP error will output "0", so if we get a "0" response, we return false to output an error message
        return true;
    }

    /**
     * Realizes the actions to send a message on the distant server
     *
     * This method is invoked whenever the user clicks the "SEND" button
     * on the {@link /fxml/contactView.fxml}
     */
    private void doSendMessage() {
        //boolean succeeded;
        Notifications notifications;
        ImageView icon;

        // if user's data are good
        if (checkUserData()) {
            /*
            This piece of code was meant to start the mail storage process in database,
            but the extension was finally refused.. So lets' replace it with a nice alert telling the user
            he successfully filled the form

            succeeded = sendMessage();
            // PHP returned 0, meaning that an error has occurred on the distant server
            if (!succeeded) {
                alert.setHeaderText("Erreur interne.");
                alert.setContentText("Une erreur interne est survenue, veuillez réessayer s.v.p.");
                alert.show();
            }
            */
            icon = new ImageView(new Image("/images/checked.png"));
            notifications = Notifications.create()
                    .title("Envoi réussi")
                    .text("Votre message a été envoyé avec succès au magasin.")
                    .graphic(icon)
                    .hideAfter(Duration.seconds(5))
                    .darkStyle()
                    .position(Pos.BOTTOM_RIGHT);


            contactEmail.clear();
            contactFirstName.clear();
            contactLastName.clear();
            contactMessage.clear();

            notifications.show();
        }
        else {
            icon = new ImageView(new Image("/images/cancel.png"));
            notifications = Notifications.create()
                    .title("Echec de l'envoi")
                    .text("Les données entrées dans les champs de contact sont erronées.")
                    .graphic(icon)
                    .hideAfter(Duration.seconds(5))
                    .darkStyle()
                    .position(Pos.BOTTOM_RIGHT);

            notifications.show();
        }
    }
}
