package fr.si3.ihm.projet.controllers;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * Created by Theo on 11/03/2017.
 */
public abstract class SuperAnimator extends SuperController {

    protected Transition translateAndFade(Node node, int byX, boolean in, Duration duration) {
        return new ParallelTransition(translate(node, byX, duration), fade(node, in ? 0 : 1, in ? 1 : 0, duration));
    }

    protected Transition translate(Node node, int byX, Duration duration) {
        TranslateTransition t1 = new TranslateTransition(duration, node);
        t1.setByX(byX);
        return t1;
    }

    protected Transition fade(Node node, int from, int to, Duration duration) {
        FadeTransition f1 = new FadeTransition(duration, node);
        f1.setFromValue(from);
        f1.setToValue(to);
        return f1;
    }

    protected Transition sliderSwap(ImageView imageView, Image image, int byX, Duration duration, EventHandler<ActionEvent> eventHandler) {
        Transition t1 = translateAndFade(imageView, byX, false, duration);
        Transition t2 = translate(imageView, -2 * byX, Duration.millis(1));
        Transition t3 = translateAndFade(imageView, byX, true, duration);

        t1.setOnFinished(event -> {
            imageView.setImage(image);
            t2.play();
        });

        t2.setOnFinished(event -> t3.play());

        t3.setOnFinished(eventHandler);

        return t1;
    }

    protected Transition getFirstBasicCarTrans(Node node, int size, int spacement, Duration duration, EventHandler<ActionEvent> eventHandler) {

        Transition t1 = translate(node, -(size + spacement), duration);

        Transition t2 = fade(node, 1, 0, Duration.millis(1));

        Transition t3 = translate(node, size + spacement, Duration.millis(1));

        t1.setOnFinished(event -> t2.play());

        t2.setOnFinished(event -> t3.play());

        t3.setOnFinished(eventHandler);

        return t1;

    }

    protected Transition getSecondBasicCarTrans(Node node) {
        return fade(node, 0, 1, Duration.millis(1));
    }

    protected Transition getFirstBorderCarTrans(Node node, int size, int spacement, Duration duration, EventHandler<ActionEvent> eventHandler) {
        Transition t1 = translateAndFade(node, -(size + spacement), false, duration.divide(2));

        Transition t2 = translate(node, (size + spacement)*7, Duration.millis(1));

        Transition t3 = translate(node, 0, Duration.millis(1));
        t3.setOnFinished(eventHandler);

        ParallelTransition p1 = new ParallelTransition(t2, t3);

        Transition t4 = translateAndFade(node, -(size + spacement), true, duration.divide(2));

        t1.setOnFinished(event -> p1.play());

        p1.setOnFinished(event -> t4.play());

        return t1;
    }
    
    protected Transition getSecondBorderCarTrans(Node node, int size, int spacement) {
        Transition t1 = fade(node, 1, 0, Duration.millis(1));

        Transition t2 = translate(node, -(size + spacement)*5, Duration.millis(1));

        Transition t3 = fade(node, 0, 1, Duration.millis(1));

        t1.setOnFinished(event -> t2.play());

        t2.setOnFinished(event -> t3.play());

        return t1;
    }
}
