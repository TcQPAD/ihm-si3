package fr.si3.ihm.projet.controllers;

import fr.si3.ihm.projet.models.ProductModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Defines the controller
 * for the "Nos Produits > Menus" view
 * of the application
 */
public class ProductMenuController extends ProductSubViewAnimator {

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        productModel = new ProductModel(ProductModel.MENU_JSON_FILE_NAME);
        initNavigationNode();
        finalizeView();
    }
}
