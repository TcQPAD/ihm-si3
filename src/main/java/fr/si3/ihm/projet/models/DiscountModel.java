package fr.si3.ihm.projet.models;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

/**
 * Defines the model
 * for the "Bons Plans" view
 * of the application
 */
public class DiscountModel {

    // category titles
    private static String imageSliderTitleText = "Nos Bons Plans";
    private static String imageCarouselTitleText = "Nos Partenaires";

    // discounts HyperLinks texts
    private static String[] discountTexts = {
            "Burger",
            "Burger Poisson",
            "Baguette",
            "Salade",
    };

    // discounts images
    private static Image[] discountImages = {
            new Image("/images/discount1.jpg"),
            new Image("/images/discount2.jpe"),
            new Image("/images/discount3.jpg"),
            new Image("/images/discount4.jpe")
    };

    // partners images list
    private static ObservableList<Image> partners;
    private static ListChangeListener<? super Image> lastListener;

    static {
        if (partners == null) {
            partners = FXCollections.observableArrayList();
            partners.add(new Image("/images/amazon.png"));
            partners.add(new Image("/images/dropbox.png"));
            partners.add(new Image("/images/google.png"));
            partners.add(new Image("/images/java.png"));
            partners.add(new Image("/images/twitter.png"));
            partners.add(new Image("/images/wordpress.png"));
            partners.add(new Image("/images/paypal.png"));
            partners.add(new Image("/images/mastercard.png"));
            partners.add(new Image("/images/visa.png"));
        }
    }

    public DiscountModel() {
    }

    /**
     * Adds a listener to the partner list
     *
     * @param listener the listener to be added to the list
     */
    public void addListener(ListChangeListener<? super Image> listener) {
        if(lastListener != null)
            partners.removeListener(lastListener);
        partners.addListener(lastListener = listener);
    }

    /**
     * Removes a listener to the partner list
     *
     * @param listener the listener to be removed to the list
     */
    public void removeListener(ListChangeListener<? super Image> listener) {
        partners.removeListener(listener);
    }

    /**
     * Getter for the discount image i
     *
     * @param i the image to be returned
     * @return the discount image i
     */
    public Image getDiscountImage(int i) {
        return discountImages[i];
    }

    /**
     * Getter for the partners list
     *
     * @return the partners list
     */
    public ObservableList<Image> getPartners() {
        return partners;
    }

    /**
     * Adds a partner image to the current partners
     *
     * @param image the image to be added to the partners
     */
    public void addPartner(Image image) {
        partners.add(image);
    }

    /**
     * Adds a partner image to the current partners
     *
     * @param path the image path to be added to the partners
     */
    public void addPartner(String path) {
        partners.add(new Image(path));
    }

    /**
     * Removes a partner
     *
     * @param index the index of the partner to be removed
     * @return the removed element
     */
    public Image removePartner(int index) {
        return partners.remove(index);
    }

    /**
     * Getter for the image slider title
     *
     * @return the image slider title
     */
    public String getImageSliderTitleText() {
        return imageSliderTitleText;
    }

    /**
     * Getter for the image carousel title
     *
     * @return the image carousel title
     */
    public String getImageCarouselTitleText() {
        return imageCarouselTitleText;
    }

    /**
     * Getter for the discount text i
     *
     * @param i the text to be return
     * @return the discount text i
     */
    public String getDiscountText(int i) {
        return discountTexts[i];
    }

    /**
     * Setter for the image slider title
     *
     * @param imageSliderTitleText the image slider title
     */
    public void setImageSliderTitleText(String imageSliderTitleText) {
        DiscountModel.imageSliderTitleText = imageSliderTitleText;
    }

    /**
     *  Setter for the image carousel title
     *
     * @param imageCarouselTitleText the image carousel title
     */
    public void setImageCarouselTitleText(String imageCarouselTitleText) {
        DiscountModel.imageCarouselTitleText = imageCarouselTitleText;
    }

    /**
     * Sets the pair discount image / discount text 1
     *
     * @param discountImage the discount image path
     * @param discountText the discount text
     */
    public void setDiscount(String discountImage, String discountText, int i) {
        discountImages[i] = new Image(discountImage);
        discountTexts[i] = discountText;
    }
}
