package fr.si3.ihm.projet.models;

import org.json.JSONObject;

/**
 * Defines the model
 * for the "Récupérer un produit" view
 * of the application
 */
public class CustomerOrderModel {

    private JSONObject orders;

    public static final String ORDER_KEY = "order_key";
    public static final String ORDER_PRODUCT = "order_product";
    public static final String ORDER_DESCRIPTION = "order_description";
    public static final String ORDER_CUSTOMER = "order_customer";
    public static final String ORDER_PRICE = "order_price";
    public static final String ORDER_DATE = "order_date";

    public static final String ORDER_IMAGE = "order_image";


    public CustomerOrderModel() {
        this.orders = new JSONObject();
        populateOrders();
    }


    /**
     * Populates the {@link this#orders} JSONObject,
     * with fake data, in order to create fake orders
     * with given IDs, products etc...
     *
     * This JSONObject is used to display different orders,
     * in function of the order ID the customer entered
     * in the corresponding {@link javafx.scene.control.TextField}
     * in the view associated to the {@link fr.si3.ihm.projet.controllers.RetrieveProductController}
     * controller.
     */
    private void populateOrders() {
        JSONObject orderFirst = new JSONObject();
        JSONObject orderSecond = new JSONObject();

        orderFirst.put(ORDER_PRODUCT, "Le burger Romain");
        orderFirst.put(ORDER_DESCRIPTION, "Cet hamburger confectionné avec des produits locaux est destiné aux amoureux " +
                "de la cuisine relevée. Composé d'une tranche de poulet pané de 300g, " +
                "d'oignons frits, de fromage ainsi que d'épices, il vous portera dans l'exotisme.");
        orderFirst.put(ORDER_CUSTOMER, "Jean Dupont - jean_dupont@gmail.com");
        orderFirst.put(ORDER_PRICE, "10,50 €");
        orderFirst.put(ORDER_DATE, "01/03/2017");
        orderFirst.put(ORDER_KEY, "123456789");

        orderFirst.put(ORDER_IMAGE, "/images/retrieved-product.jpg");

        orderSecond.put(ORDER_PRODUCT, "Le burger Montagnard");
        orderSecond.put(ORDER_DESCRIPTION, "Cet hamburger confectionné avec des produits des Alpes françaises vous fera vivre les sensations du ski." +
                "Composé d'un steak de boucher de 325g, " +
                "de morbier, ainsi que de charcuterie des Alpes, ce hamburger ravira les amoureux de la cuisine du terroir français !");
        orderSecond.put(ORDER_CUSTOMER, "Pierre Dupuis - pierre-dupuis@hotmail.com");
        orderSecond.put(ORDER_PRICE, "11,50 €");
        orderSecond.put(ORDER_DATE, "27/02/2017");
        orderSecond.put(ORDER_KEY, "987654321");

        orderSecond.put(ORDER_IMAGE, "/images/retrieved-product-2.jpeg");

        orders.put(orderFirst.getString(ORDER_KEY), orderFirst);
        orders.put(orderSecond.getString(ORDER_KEY), orderSecond);
    }

    /**
     * Gets the order registered with the given order key,
     * from the {@link this#orders} JSONObject of orders,
     * or returns null if no order can be found
     *
     * @param orderKey the key entered by the customer,
     *                 that must match one of the orders
     *                 stored in {@link this#orders}.
     *
     * @return the {@link JSONObject} describing the order
     *         orderKey, or null if no order has the given key
     */
    public JSONObject getOrder(String orderKey) {
        return orders.has(orderKey) ? orders.getJSONObject(orderKey) : null;
    }
}
