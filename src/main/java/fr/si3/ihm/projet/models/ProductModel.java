package fr.si3.ihm.projet.models;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Defines the model of the
 * "Produits phares" controller.
 *
 * This class is able to read a JSON file,
 * in order to retrieve products to display
 * on the view
 *
 * @implNote all data must store JSONArrays in order for this
 *           class to work properly.
 */
public class ProductModel {

    /*  product menus file */
    public static final String MENU_JSON_FILE_NAME = "/data/product_menu.json";

    /*  product starters file */
    public static final String STARTER_JSON_FILE_NAME = "/data/product_starter.json";

    /*  product meals file */
    public static final String MEAL_JSON_FILE_NAME = "/data/product_meal.json";

    /*  product drinks file */
    public static final String FINISHER_JSON_FILE_NAME = "/data/product_finisher.json";

    /*  product exclusives file */
    public static final String EXCLU_JSON_FILE_NAME = "/data/product_exclu.json";

    public static final String PRODUCT_IMAGE = "product_image";
    public static final String PRODUCT_NAME = "product_name";

    private BufferedReader jsonReader;

    private JSONArray data;

    private String openedFile;

    /**
     * Builds a ProductModel instance.
     *
     * @implNote in order to read the good file when going into
     * a view requesting this model, we must pass
     * the json file name to the constructor of this method
     *
     * @implSpec Default behavior is to retrieve data from the given file
     *
     * @param jsonName the name of the file to read.
     *                 One wants to use the file values described in this class.
     */
    public ProductModel(String jsonName) {
        jsonReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(jsonName)));
        openedFile = jsonName;
        retrieveFileData();
    }

    /**
     * Gets the name of the json file
     * opened in the buffered reader.
     *
     * @return the name of the json file
     *         being read
     */
    public String getOpenedFile() {
        return openedFile;
    }

    /**
     * Reads the file storing
     * the products exclusives
     * and returns a data structure containing
     * all the data.
     *
     */
    public void retrieveFileData() {
        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = jsonReader.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        this.data = new JSONArray(sb.toString());
    }

    /**
     * Gets the data retrieved from file
     * @return the data retrieved from file as
     *         a {@link JSONArray}
     */
    public JSONArray getData() {
        return data;
    }

}
