package fr.si3.ihm.projet.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines the model of the
 * search bar in the menu bar,
 * which will be used in the {@link fr.si3.ihm.projet.controllers.HomeController}.
 *
 * This class is able to read a dico file,
 * in order to retrieve research words and compare
 * them to the ones the user entered in the search bar
 *
 */
public class ResearchModel {

    private Map<String, String> viewNamesWithFxml;

    public ResearchModel() {
        this.viewNamesWithFxml = new HashMap<>();
        populateMap();
    }

    /**
     * Creates all entries for the
     * {@link this#viewNamesWithFxml} map.
     *
     */
    private void populateMap() {
        viewNamesWithFxml.put("Accueil", "/fxml/homeView.fxml");

        viewNamesWithFxml.put("Produits", "/fxml/productView.fxml");
        viewNamesWithFxml.put("Bons plans", "/fxml/discountView.fxml");
        viewNamesWithFxml.put("Menus", "/fxml/productMenuView.fxml");
        viewNamesWithFxml.put("Entrées", "/fxml/productStarterView.fxml");
        viewNamesWithFxml.put("Plats", "/fxml/productMealView.fxml");
        viewNamesWithFxml.put("Desserts", "/fxml/productFinisherView.fxml");
        viewNamesWithFxml.put("Récupérer un produit", "/fxml/retrieveProductView.fxml");

        viewNamesWithFxml.put("Nous Trouver", "/fxml/findUsView.fxml");

        viewNamesWithFxml.put("Nous contacter", "/fxml/contactView.fxml");
        viewNamesWithFxml.put("Mentions Légales", "/fxml/mentionsView.fxml");
    }

    /**
     * Gets the research map
     * @return the map used to research in the
     *         application
     */
    public Map<String, String> getViewNamesWithFxml() {
        return this.viewNamesWithFxml;
    }

    /**
     * Gets the value associated to the
     * given key.
     *
     * The value of the key is either a path to a FXML
     * file, or null if the key doesn't exist in the hashmap.
     *
     * @param research the research value made by the user.
     *
     * @return a String corresponding to the FXML file to load if the key exists,
     *         or null if it doesn't.
     */
    public String getFxmlResearched(String research) {
        return this.viewNamesWithFxml.containsKey(research) ? this.viewNamesWithFxml.get(research) : null;
    }
}
