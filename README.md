## Synopsis

Projet IHM-SI3 de Polytech Nice Sophia, 2016-2017.

## Installation

Afin de pouvoir lancer ce projet, vous devez posséder un environnement
Java8 ainsi que l'outil d'intégration maven qui sera utilisé pour installer
les dépendances du projet et exécuter le programme.

### Note sur les polices 

Vous trouverez dans le dossier `src/main/resources/styles/` une suite de polices qui sont utilisées pour ce projet.
Nous vous conseillons d'installer ces polices pour un affichage optimal dans le logiciel.

Pour installer les polices, veuillez exécuter les fichiers `Font.tff`, puis choisir `Installer`

## Côté administrateur

### Fichiers des vues produits

Les fichiers utilisés pour stocker les différents produits du magasin
sont stockés dans `/data/`.

Ces fichiers sont au format `JSON` et nécessitent de suivre la structure suivante :

`[ {"exclu1":
     [
       {"description" : "sony",
         "price" : "$1000"},
       {"image_path" : "/images/image_produit.png"}
     ], ... ]`

C'est à dire :

`[JSONArray > {JSONObject > JSONArray}, {JSONObject > JSONArray}, ..., {JSONObject > JSONArray}]`

## Contributors

 * Maxime Flament
 * Théo Frasquet